#include <stdio.h>
#include <stdbool.h>

#include "testframework.h"
#include "testcases.h"

void printPassOrFail(bool pass);

int main(int argc, char *argv[])
{
  printPassOrFail(testcase_calc_resistance_1());
  printPassOrFail(testcase_calc_resistance_2());
  printPassOrFail(testcase_calc_resistance_3());
  printPassOrFail(testcase_calc_resistance_4());
  printPassOrFail(testcase_calc_resistance_5());

  printPassOrFail(testcase_calc_power_r_1());
  printPassOrFail(testcase_calc_power_r_2());

  printPassOrFail(testcase_calc_power_i_1());

  printPassOrFail(testcase_e_resistance_1());
  printPassOrFail(testcase_e_resistance_2());
  return 0;
}

void printPassOrFail(bool pass)
{
  if(pass)
  {
    printf("PASS\n");
  }
  else
  {
    printf("FAIL\n");
  }
}
