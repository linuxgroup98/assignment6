#ifndef TESTCASES_INCLUDE_GUARD_H
#define TESTCASES_INCLUDE_GUARD_H
#include <stdbool.h>

bool testcase_calc_resistance_1();
bool testcase_calc_resistance_2();
bool testcase_calc_resistance_3();
bool testcase_calc_resistance_4();
bool testcase_calc_resistance_5();

bool testcase_calc_power_r_1();
bool testcase_calc_power_r_2();

bool testcase_calc_power_i_1();

bool testcase_e_resistance_1();
bool testcase_e_resistance_2();

#endif  //TESTCASES_INCLUDE_GUARD_H
