#include <stdio.h>
#include <stdlib.h> //Needed for malloc
#include "electrotest.h"
#include "power.h"
#include "resistance.h"
#include "component.h"

void handleCalculateTotalResistance()
{
  int nof_resistors;
  char conn;
  int i;

  printf("Beräkna den totala resistansen för olika kopplade resistanser.\n");

  printf("Hur många motstånd vill du ange? ");
  scanf("%d", &nof_resistors);

  float* resistor_array_p = (float*)malloc(sizeof(float)*nof_resistors);

  for (i = 0; i < nof_resistors; i++)
  {
    printf("Ange motstånd %d i ohm: ", i+1);
    scanf("%f", &resistor_array_p[i]);
  }

  printf("Vilken koppling vill du beräkna för (P=parallellkoppling, S=seriekoppling)?");
  scanf(" %c", &conn);

  printf("Totala motståndet blir %.2f ohm.\n", calc_resistance((int)nof_resistors, conn, resistor_array_p));
  free(resistor_array_p);
}

void handleCalculatePowerFromVoltadeAndCurrent()
{
  float voltage;
  float current;
  printf("Beräkna effekt över ett motstånd baserat på känd spänning och ström.\n");

  printf("Ange spänning i volt: ");
  scanf("%f", &voltage);

  printf("Ange strömmen i ampere: ");
  scanf("%f", &current);

  printf("Effekten blir %.2f watt.\n", calc_power_i(voltage, current));
}

void handleCalculatePowerFromVoltadeAndResistance()
{
  float voltage;
  float resistance;
  printf("Beräkna effekt över ett motstånd baserat på känd spänning och motståndsvärde.\n");

  printf("Ange spänning i volt: ");
  scanf("%f", &voltage);

  printf("Ange motståndet i ohm: ");
  scanf("%f", &resistance);

  printf("Effekten blir %.2f watt.\n", calc_power_r(voltage, resistance));
}

void handleCalculateReplacementE12Resistors()
{
  float resistor;
  int nof_replacement_resistors_needed;
  int i;
  float* resistor_array_p = (float*)malloc(sizeof(float)*3);
  resistor_array_p[0] = (float)0;
  resistor_array_p[1] = (float)0;
  resistor_array_p[2] = (float)0;

  printf("Hitta lämpliga ersättningsmotstånd ur E12-serien för ett känt motstånd.\n");

  printf("Ange motståndet att ersätta i ohm: ");
  scanf("%f", &resistor);

  nof_replacement_resistors_needed = e_resistance(resistor, resistor_array_p);

  printf("%d ersättningsmotstånd ur E12-serien behövs.\n", nof_replacement_resistors_needed);
  for (i = 0; i < nof_replacement_resistors_needed; i++)
  {
    printf("Ersättningsmotstånd %d är %.2f\n", i+1, resistor_array_p[i]);
  }

  free(resistor_array_p);
}

void handleMainMenu()
{
  char main_menu_choise;

  do
  {
    printf("\n\n\n***************************  Huvudmenu  ***************************\n\n");
    printf("1. Beräkna den totala resistansen för olika kopplade resistanser.\n");
    printf("2. Beräkna effekt över ett motstånd baserat på känd spänning och ström.\n");
    printf("3. Beräkna effekt över ett motstånd baserat på känd spänning och motståndsvärde.\n");
    printf("4. Hitta lämpliga ersättningsmotstånd ur E12-serien för ett känt motstånd.\n");
    printf("\n0. Avsluta programmet.\n");

    printf(": ");
    scanf(" %c", &main_menu_choise);
    switch(main_menu_choise)
    {
      case '1':
        handleCalculateTotalResistance();
        break;
      case '2':
        handleCalculatePowerFromVoltadeAndCurrent();
        break;
      case '3':
        handleCalculatePowerFromVoltadeAndResistance();
        break;
      case '4':
        handleCalculateReplacementE12Resistors();
        break;
      default:
        break;
    }
  } while (main_menu_choise != '0');
}

int main(int argc, char *argv[])
{
  handleMainMenu();
  return 0;
}


