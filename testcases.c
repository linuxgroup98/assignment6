#include <stdio.h>
#include <stdlib.h> //Needed for malloc
#include <float.h> //Needed for FLT_MAX

#include "testcases.h"

#include "power.h"
#include "resistance.h"
#include "component.h"

#define round(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))

bool testcase_calc_resistance_1()
{
  printf("%s                     ", __FUNCTION__);

  bool retVal;
  float* resistor_array_p = (float*)malloc(sizeof(float)*3);

  resistor_array_p[0] = (float)100;
  resistor_array_p[1] = (float)200;
  resistor_array_p[2] = (float)300;

  retVal = (bool)(round(calc_resistance(3, 'S', resistor_array_p)) == 600);
  free(resistor_array_p);
  return retVal;
}

bool testcase_calc_resistance_2()
{
  printf("%s                     ", __FUNCTION__);

  bool retVal;
  float* resistor_array_p = (float*)malloc(sizeof(float)*3);
  resistor_array_p[0] = (float)100;
  resistor_array_p[1] = (float)300;
  resistor_array_p[2] = (float)300;

  retVal = (bool)(round(calc_resistance(3, 'P', resistor_array_p)) == 60);
  free(resistor_array_p);
  return retVal;
}

bool testcase_calc_resistance_3()
{
  printf("%s                     ", __FUNCTION__);

  //    Biblioteket får inte krascha om en "nollpekare" skickas till funktioen, dvs om array=0.
  //    Om argumenten är felaktiga skall funktionen returnera -1.
  return (calc_resistance(3, 'P', NULL) == -1);
}

bool testcase_calc_resistance_4()
{
  printf("%s                     ", __FUNCTION__);

  //    Biblioteket får inte krascha om en "nollpekare" skickas till funktioen, dvs om array=0.
  //    Om argumenten är felaktiga skall funktionen returnera -1.
  return (calc_resistance(3, 'P', 0) == -1);
}


bool testcase_calc_resistance_5()
{
  printf("%s                     ", __FUNCTION__);

  bool retVal;
  float* resistor_array_p = (float*)malloc(sizeof(float)*3);

  resistor_array_p[0] = (float)100;
  resistor_array_p[1] = (float)0;
  resistor_array_p[2] = (float)300;

  //Värdet 0 skall returneras om något motstånd är noll vid parallellkoppling, dvs  R1||R2=0, om R1 eller R2 är 0Ohm.
  retVal = (bool)(calc_resistance(3, 'P', resistor_array_p) == 0);

  free(resistor_array_p);
  return retVal;
}



bool testcase_calc_power_r_1()
{
  printf("%s                     ", __FUNCTION__);

  return (calc_power_r(10, 10) == 10);
}


bool testcase_calc_power_r_2()
{
  printf("%s                     ", __FUNCTION__);

  //A resistance of 0 ohms will, if not handled, cause a zero division.
  return (calc_power_r(10, 0) == FLT_MAX);
}


bool testcase_calc_power_i_1()
{
  printf("%s                     ", __FUNCTION__);

  return (calc_power_i(10, 10) == 100);
}



bool testcase_e_resistance_1()
{
  printf("%s                     ", __FUNCTION__);

  bool retVal;
  int nofResistors; 
  float* resistor_array_p = (float*)malloc(sizeof(float)*3);
  resistor_array_p[0] = (float)0;
  resistor_array_p[1] = (float)0;
  resistor_array_p[2] = (float)0;

  nofResistors = e_resistance(470, resistor_array_p);

  retVal = (bool)(nofResistors == 1 && round(resistor_array_p[0]) == 470 && round(resistor_array_p[1]) == 0 && round(resistor_array_p[2]) == 0);

  free(resistor_array_p);
  return retVal;
}

bool testcase_e_resistance_2()
{
  printf("%s                     ", __FUNCTION__);

  bool retVal;
  int nofResistors; 
  float* resistor_array_p = (float*)malloc(sizeof(float)*3);
  resistor_array_p[0] = (float)0;
  resistor_array_p[1] = (float)0;
  resistor_array_p[2] = (float)0;

  nofResistors = e_resistance(3480, resistor_array_p);  //3300 + 180 = 3480

  retVal = (bool)(nofResistors == 2 && round(resistor_array_p[0]) == 3300 && round(resistor_array_p[1]) == 180 && round(resistor_array_p[2]) == 0);

  free(resistor_array_p);
  return retVal;
}



