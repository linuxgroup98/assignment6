CC    = gcc

FLAGS        =
CFLAGS       = -Wall -Wl,--no-as-needed
DEBUGFLAGS   = -g

TARGET  = electrotest
BINDIR  = /usr/bin
LIBDIR  = /usr/lib

lib: power resistance component

power: lib/power.c lib/power.h
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -c -fPIC -Ilib lib/power.c
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -shared -fPIC -o lib/libpower.so power.o
	-rm -f power.o

resistance: lib/resistance.c lib/resistance.h
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -c -fPIC -Ilib lib/resistance.c
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -shared -fPIC -o lib/libresistance.so resistance.o
	-rm -f resistance.o

component: lib/component.c lib/component.h
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -c -fPIC -Ilib lib/component.c
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -shared -fPIC -o lib/libcomponent.so component.o
	-rm -f component.o

all: lib
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -o electrotest electrotest.c -Llib -Ilib -Wl,-rpath=lib -lpower -lresistance -lcomponent -lm

test: lib
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -c testcases.c -Ilib -Llib -Wl,-rpath=lib -lpower -lresistance -lcomponent -lm
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -c testframework.c -Ilib -Llib -Wl,-rpath=lib -lpower -lresistance -lcomponent -lm
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -o testframework testcases.o testframework.o -Ilib -Llib -Wl,-rpath=lib -lpower -lresistance -lcomponent -lm


install: lib
	cp -f lib/*.so $(DESTDIR)/$(LIBDIR)
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -o electrotest electrotest.c -L$(DESTDIR)/$(LIBDIR) -Ilib -Wl,-rpath=$(DESTDIR)/$(LIBDIR) -lpower -lresistance -lcomponent -lm
	install -D $(TARGET) $(DESTDIR)/$(BINDIR)/$(TARGET)


uninstall:
	-rm -f $(DESTDIR)/$(BINDIR)/$(TARGET)
	-rm -f $(DESTDIR)/$(LIBDIR)/libcomponent.so
	-rm -f $(DESTDIR)/$(LIBDIR)/libpower.so
	-rm -f $(DESTDIR)/$(LIBDIR)/libresistance.so

clean:
	-rm -f lib/*.so
	-rm -f *.o
	-rm -f electrotest
	-rm -f testframework



