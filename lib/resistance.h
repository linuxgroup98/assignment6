#ifndef RESISTANCE_INCLUDE_GUARD_H
#define RESISTANCE_INCLUDE_GUARD_H

float calc_resistance(int count, char conn, float *array);


#endif  //RESISTANCE_INCLUDE_GUARD_H
