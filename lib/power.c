#include <stdio.h>
#include <float.h> //Needed for FLT_MAX

float calc_power_r(float volt, float resistance)
{
  float power;

  if(resistance == 0)
  {
    power = FLT_MAX;
  }
  else
  {
    power = (float)(volt * volt / resistance);
  }

  return power;
};

float calc_power_i(float volt, float current)
{
  return (float)(volt * current);
};


