#include <stdio.h>

float calc_resistance(int count, char conn, float *array)
{
  float resistance = 0;

  if(array == NULL) {
    //Check for null
	printf("Array == NULL\n");
    return -1;
  }
  if(count <= 0 || sizeof(array) <= 0) {
    //Check for invalid values
	printf("Invalid Values\n");
    return -1;
  }
	
  if(conn == 'P' || conn == 'p'){			//If parallell
    int i = 0;
    for (i = 0; i < count; i++){
      if(array[i] == 0){
        return 0;			// If resistance are zero return zero.
      } else {
        resistance += 1/array[i]; 	//Add 1/(component resistance)
      }
    }	
    
    resistance = 1/resistance;
    	
  } else if (conn == 'S' || conn == 's'){		//If serial
    int i = 0;
    for (i = 0; i < count; i++){
      resistance += array[i];		//Add component resistance
    }
  } else {				//Check for valid conn value
    return -1;
  }

  return resistance;
};



