#include <stdio.h>
#include <math.h>
#include "component.h"


float getResistor(float resist)
{
        float e12Resistors[] = {1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2};
        int magi = 0;
        float quota = 0;
        magi = log10(resist);
        int i = 11;

        while(quota < 1)
        {
                quota = resist / (e12Resistors[i] * pow(10, magi));
                i--;
        }
        return (e12Resistors[(i+1)] * pow(10, magi));
}

int e_resistance(float orig_resistance, float *res_array )
{
        int count = 0;
        float resistance = 0;
        resistance = orig_resistance;
        while(resistance >= 1.0 && count < 3)
        {
                *(res_array + count) = getResistor(resistance);
                resistance -= *(res_array + count);
                count++;
        }
        return count;
}

