#ifndef POWER_INCLUDE_GUARD_H
#define POWER_INCLUDE_GUARD_H

float calc_power_r(float volt, float resistance);

float calc_power_i(float volt, float current);


#endif  //POWER_INCLUDE_GUARD_H
